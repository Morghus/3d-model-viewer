var path = require('path');

module.exports = {
    devtool: 'source-map',
    entry: {
        app: './src/app',
        ModelViewer: './src/ModelViewerApp'
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].js',
        library: ['[name]']
    },
    module: {
        loaders: [
            { test: /\.(glsl|frag|vert)$/, loader: 'raw-loader', exclude: /node_modules/ },
            { test: /\.(glsl|frag|vert)$/, loader: 'glslify-loader', exclude: /node_modules/ },
            {
                test: /\.less$/,
                loader: "style-loader!css-loader!less-loader",
                exclude: /node_modules/
            },
            {
                test: /\.css/,
                loader: "style-loader!css-loader"
            },
            {
                test: /\.worker\.js$/,
                loader: 'worker-loader',
                exclude: /node_modules/
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    cacheDirectory: '.babel-cache',
                    presets: ['env'],
                }
            }
        ]
    }
};
