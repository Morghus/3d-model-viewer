#!/usr/bin/env bash

set -e

compress_if_newer() {
    local gzfile="$2"
    local file="$1"
    local command="$3"
    if [ ! -f "$gzfile" -o "$file" -nt "$gzfile" ]; then
        trap "echo Cleaning up partly compressed file: '$gzfile'; rm '$gzfile'" EXIT
        echo Compressing "\"$file\"" using "\"$command\""
        eval ${command}
        trap - EXIT
    fi
}


while read file; do
    compress_if_newer "$file" "$file.gz" "zopfli -v \"$file\""
    #compress_if_newer "$file" "$file.br" "bro --input \"$file\" --output \"$file.br\" --verbose"
done < <(find . -type f -iname *.obj; ls dist/*.js)