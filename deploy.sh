#!/usr/bin/env bash

set -e

if [ "$1" == "" ]; then
    echo "Error: specify host"
    exit 1
fi

if [ "$2" == "" ]; then
    echo "Error: specify dist path"
    exit 1
fi

rsync -P -rtv \
    --delete \
    --exclude node_modules \
    --exclude .git \
    --exclude .idea \
    --exclude .babel-cache \
    --delete-excluded \
    . "$1":"$2"
ssh "$1" chmod -R a+rx "$2"
