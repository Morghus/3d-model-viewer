import * as THREE from 'three'
import lerp from './utils/lerp'
import * as _ from 'lodash'
import Component from './lib/Component'
import RenderLoopEvent from './lib/RenderLoopEvent'
import WindowResizeEvent from './lib/WindowResizeEvent'
import LowFpsEvent from "./lib/LowFpsEvent";

/**
 * @extends Component
 * @property {THREE.PerspectiveCamera} camera
 * @property {THREE.Scene} scene
 * @property {THREE.WebGLRenderer} renderer
 */
export default class engine extends Component {
    constructor() {
        super()
        this.frameRequest = null
        this.activeRenderer = this.renderer = this.createRenderer()
        this.renderer.clear()
        this.camera = this.createCamera()
        this.scene = this.createScene()
        this.manager = new THREE.LoadingManager()
        this.manager.onProgress = (item, loaded, total) => {
            console.log(item, loaded, total)
        };

        document.body.appendChild(this.renderer.domElement)

        document.addEventListener('mousemove', this.onDocumentMouseMove.bind(this), false)
        window.addEventListener('resize', this.onWindowResize.bind(this), false)

        document.addEventListener("visibilitychange", (event) => this.handleVisibilityChange(event), false);
    }

    /** @returns {THREE.WebGLRenderer} */
    createRenderer() {
        let {width, height} = this.getSize()
        let renderer = new THREE.WebGLRenderer()
        renderer.setPixelRatio(window.devicePixelRatio)
        renderer.setSize(width, height)
        renderer.shadowMap.enabled = true
        renderer.shadowMap.type = THREE.PCFSoftShadowMap
        return renderer
    }

    /** @returns {THREE.PerspectiveCamera} */
    createCamera() {
        let {width, height} = this.getSize()
        let camera = new THREE.PerspectiveCamera(35, width / height, 0.1, 100)
        camera.position.z = 1
        return camera
    }

    /** @returns {THREE.Scene} */
    createScene() {
        return new THREE.Scene()
    }
    onWindowResize() {
        const {width, height} = this.getSize()
        this.windowHalfX = height / 2
        this.windowHalfY = width / 2

        this.camera.aspect = width / height
        this.camera.updateProjectionMatrix()

        this.renderer.setSize(width, height);
        if (this.activeRenderer !== null && typeof this.activeRenderer.setSize === 'function') {
            this.activeRenderer.setSize(width, height)
        }

        this.trigger(new WindowResizeEvent(this, {width, height}))
    }
    onDocumentMouseMove(event) {
        let mouseX = ( event.clientX - this.windowHalfX ) / 2
        let mouseY = ( event.clientY - this.windowHalfY ) / 2
    }
    fpsWarningChecker() {
        const fps = this.calculateAvgFps()
        if (fps < 30) {
            this.trigger(new LowFpsEvent(this, fps))
        }
    }
    renderLoop() {
        if (!this.running) {
            return;
        }

        const now = performance.now()

        lerp.tick();

        const delta = now - this.lastRender;
        this.trigger(new RenderLoopEvent(this, {delta, now}));

        this.activeRenderer.render(this.scene, this.camera)

        if (this.frame > 1) {
            this.avgFps.unshift(1 / delta * 1000)
            while (this.avgFps.length > 60) {
                this.avgFps.pop()
            }
        } else if (this.frame === 1) {
            this.fpsWarningFirst = setTimeout(() => this.fpsWarningChecker(), 200)
            this.fpsWarning = setInterval(() => this.fpsWarningChecker(), 1000)
        }
        this.lastRender = now
        this.frame++
    }

    requestAnimationFrame() {
        if (this.frameRequest) {
            return
        }
        this.frameRequest = requestAnimationFrame(() => {
            this.frameRequest = null
            this.renderLoop()
            this.requestAnimationFrame()
        })
    }

    cancelAnimationFrame() {
        cancelAnimationFrame(this.frameRequest)
        this.frameRequest = null
    }

    calculateAvgFps() {
        return _.reduce(this.avgFps, (a, b) => a + b) / this.avgFps.length
    }

    getSize() {
        return { width: window.innerWidth, height: window.innerHeight}
    }
    setAlternateRenderer(alternateRenderer) {
        this.activeRenderer = alternateRenderer || this.renderer
    }
    start() {
        if (this.started) {
            return
        }
        this.started = true
        this.avgFps = []
        if (!document.hidden) {
            this.resume()
        }
    }
    stop() {
        this.started = false
        this.pause()
    }

    pause() {
        this.cancelAnimationFrame()
        this.running = false
        clearInterval(this.fpsWarning)
        clearTimeout(this.fpsWarningFirst)
        this.fpsWarning = null
    }

    resume() {
        if (!this.started) {
            return
        }
        this.lastRender = performance.now()
        this.frame = 0
        this.running = true
        this.renderLoop()
        this.requestAnimationFrame()
    }

    handleVisibilityChange() {
        if (document.hidden) {
            this.pause()
        } else {
            this.resume()
        }
    }
}