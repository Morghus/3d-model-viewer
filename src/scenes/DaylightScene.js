"use strict"
import Scene from './Scene'
import THREE from 'three'
import RenderLoopEvent from "../lib/RenderLoopEvent"
import Listener from "../lib/Listener"
import 'imports-loader?THREE=three!three/examples/js/Mirror'
import '../lib/three/NodeMaterials'
import WindowResizeEvent from "../lib/WindowResizeEvent"
import ProjectionNode from '../lib/three/ProjectionNode'
import SkySphere from './objects/SkySphere'

/**
 * @extends Scene
 * @property {THREE.Mesh} skybox
 */
export default class DaylightScene extends Scene {
    constructor() {
        super()
        var ambient = new THREE.HemisphereLight(0xffffff, 0x808080, 0.4)
        this.scene.add(ambient);

        [[5,5,5], [-5,5,5], [-5,5,-5], [5,5,-5]].forEach((args) => this.scene.add(this.createDirectionalLight(args)))
        this.scene.add(this.createSpotLight())
        this.ground = this.createGround()
        this.scene.add(this.ground)

        this.shouldRenderMirror = false

        this.addListener(new Listener(RenderLoopEvent, (event) => this.renderLoop(event)))
        this.addListener(new Listener(WindowResizeEvent, (event) => this.onResize(event)))
    }

    createSpotLight() {
        const spotLight = new THREE.SpotLight(0xffffff, 0.4)
        spotLight.name = 'Directional Light'
        spotLight.position.set(0, 10, 0)
        spotLight.castShadow = true
        spotLight.shadowBias = 0.000001
        spotLight.shadow.camera.near = 5
        spotLight.shadow.camera.far = 15
        spotLight.shadow.mapSize.width = 1024
        spotLight.shadow.mapSize.height = 1024
        return spotLight
    }

    createDirectionalLight(args) {
        const dirLight = new THREE.DirectionalLight(0xffffff, 0.2)
        dirLight.name = 'Directional Light'
        dirLight.position.set.apply(dirLight.position, args)
        return dirLight
    }

    /**
     * @returns {THREE.Texture}
     */
    generateGroundAlphaTexture() {
        if (!this.groundAlphaTexture) {
            const size = 512
            const canvas = document.createElement('canvas')
            canvas.width = size
            canvas.height = size
            const context = canvas.getContext('2d')

            context.rect(0, 0, size, size)
            const halfSize = size / 2
            var gradient = context.createRadialGradient(halfSize, halfSize, 0, halfSize, halfSize, halfSize)
            gradient.addColorStop(0, '#ffffff')
            gradient.addColorStop(0.3, '#ffffff')
            gradient.addColorStop(0.7, '#000000')
            gradient.addColorStop(1, '#000000')
            context.fillStyle = gradient
            context.fill()

            this.groundAlphaTexture = new THREE.CanvasTexture(canvas)
        }
        return this.groundAlphaTexture
    }

    onAttach() {
        super.onAttach()

        if (this.component.renderer !== this.previousRenderer) {
            this.skySphere = new SkySphere(this.component.renderer)
            this.groundMirror = new THREE.Mirror( this.component.renderer, this.component.camera, {
                clipBias: 0.003,
                textureWidth: 2048,
                textureHeight: 2048
            })
            this.ground.add(this.groundMirror)
        }
        this.scene.add(this.skySphere)

        // this.component.renderer.shadowMap.autoUpdate = false
        // this.component.renderer.shadowMap.needsUpdate = true

        this.component.collisionObjects.add(this.ground)

        this.detachPromise = new Promise((resolve, reject) => {
            this.detachPromiseReject = reject
        })

        Promise.race([this.component.getModel(), this.detachPromise]).then((object) => {
            const box = new THREE.Box3()
            box.setFromObject(object)
            this.ground.position.y = box.min.y - 0.001

            object.traverse((obj) => {
                if (obj.name === 'Plane') {
                    this.replaceShadowPlaneWithMirrorProjection(obj, this.ground, this.groundMirror);
                    this.shouldRenderMirror = true
                    obj.visible = false
                }
            })
        }).catch()
    }

    replaceShadowPlaneWithMirrorProjection(obj, ground, groundMirror) {
        const shadowProjection = this.createShadowProjectionNodeFromObj(obj);

        const mirrorTexture = new THREE.InputNode('t')
        mirrorTexture.value = groundMirror.texture
        const mirrorProjection = new THREE.InputNode('m4')
        mirrorProjection.value = groundMirror.textureMatrix

        // MATERIAL
        const createMirrorMaterial = () => {
            const mtl = new THREE.PhongNodeMaterial()
            mtl.specular = new THREE.ColorNode(0xffffff)
            mtl.shininess = new THREE.FloatNode(40)
            return mtl
        }

        const mainReflectivity = 1
        const scaledInverseShadowProjection = this.adjustLevels(new THREE.OperatorNode(
            new THREE.Vector4Node(1, 1, 1),
            shadowProjection,
            THREE.OperatorNode.SUB), mainReflectivity);

        const mirrorProjectionNode = new ProjectionNode(mirrorTexture, mirrorProjection);
        const combinedShadowAndMirrorProjection = new THREE.Math3Node(
            new THREE.OperatorNode(
                scaledInverseShadowProjection,
                mirrorProjectionNode,
                THREE.OperatorNode.MUL),
            shadowProjection,
            new THREE.FloatNode(0.4),
            THREE.Math3Node.MIX)

        ground.traverse((obj) => {
            if (obj instanceof THREE.Mesh) {
                if (obj.geometry instanceof THREE.RingGeometry) {
                    const mtl = createMirrorMaterial()

                    mtl.color = combinedShadowAndMirrorProjection
                    mtl.alpha = new THREE.TextureNode(this.generateGroundAlphaTexture())
                    mtl.specular = new THREE.Vector4Node(0.5, 0.5, 0.5)
                    obj.material = mtl.build()
                } else {
                    const mtl = createMirrorMaterial()
                    mtl.color = combinedShadowAndMirrorProjection
                    mtl.specular = new THREE.OperatorNode(shadowProjection, new THREE.Vector4Node(0.5, 0.5, 0.5), THREE.OperatorNode.MUL)
                    obj.material = mtl.build()
                }
            }
        })
    }

    /**
     * This basically performs calculation (1 - v) * (1 - r) + r, scaling the output and moving it to 1.
     * @param {THREE.GLNode} shadowProjection
     * @param {number} mainReflectivity
     * @returns {THREE.GLNode}
     */
    adjustLevels(shadowProjection, mainReflectivity) {
        return new THREE.OperatorNode(
            new THREE.OperatorNode(
                new THREE.Vector4Node(1 - mainReflectivity, 1 - mainReflectivity, 1 - mainReflectivity),
                shadowProjection,
                THREE.OperatorNode.MUL),
            new THREE.Vector4Node(mainReflectivity, mainReflectivity, mainReflectivity),
            THREE.OperatorNode.ADD);
    }

    createShadowProjectionNodeFromObj(obj) {
        const lightMatrix = this.generateShadowProjectionMatrix(obj);

        const shadowNode = new THREE.InputNode('t')
        shadowNode.value = new THREE.Texture()
        shadowNode.value.image = obj.material.emissiveMap.image
        shadowNode.value.needsUpdate = true
        const shadowMatrixNode = new THREE.InputNode('m4')
        shadowMatrixNode.value = lightMatrix
        return new ProjectionNode(shadowNode, shadowMatrixNode)
    }

    generateShadowProjectionMatrix(shadowObject) {
        const boxMeshBox = new THREE.Box3()
        boxMeshBox.setFromObject(shadowObject)

        const lightCamera = new THREE.OrthographicCamera(boxMeshBox.max.x, boxMeshBox.min.x, boxMeshBox.min.z, boxMeshBox.max.z)
        lightCamera.translateY(1)

        const targetPosition = new THREE.Vector3()
        targetPosition.setFromMatrixPosition(this.ground.matrixWorld)

        lightCamera.lookAt(targetPosition)
        lightCamera.rotateZ(Math.PI / 2)
        lightCamera.updateMatrixWorld()
        lightCamera.matrixWorldInverse.getInverse(lightCamera.matrixWorld)

        const lightMatrix = new THREE.Matrix4()
        lightMatrix.set(0.5, 0.0, 0.0, 0.5,
            0.0, 0.5, 0.0, 0.5,
            0.0, 0.0, 0.5, 0.5,
            0.0, 0.0, 0.0, 1.0)

        lightMatrix.multiply(lightCamera.projectionMatrix)
        lightMatrix.multiply(lightCamera.matrixWorldInverse)
        return lightMatrix
    }

    onDetach() {
        super.onDetach()

        // this.component.renderer.shadowMap.autoUpdate = true

        this.component.collisionObjects.delete(this.ground)

        this.scene.remove(this.skySphere)

        this.previousRenderer = this.component.renderer

        this.detachPromiseReject()
    }

    /**
     * @param {RenderLoopEvent} event
     */
    renderLoop(event) {
        if (this.shouldRenderMirror) {
            this.ground.visible = false
            this.groundMirror.render()
            this.ground.visible = true
        }
    }

    /** @param {WindowResizeEvent} event */
    onResize(event) {
        if (this.groundMirror) {
            this.groundMirror.mirrorCamera.aspect = event.width / event.height
            this.groundMirror.mirrorCamera.updateProjectionMatrix()
        }
    }

    /**
     * @returns {THREE.Object3D}
     */
    createGround() {
        const materialParams = {
            color: 0x909090,
            specular: 0x505050,
            shininess: 100
        }

        const ground = new THREE.Object3D()

        const centerSize = 10

        const center = new THREE.Mesh(new THREE.PlaneGeometry(centerSize, centerSize, 1, 1), new THREE.MeshPhongMaterial(materialParams))
        center.receiveShadow = false
        center.castShadow = false
        ground.add(center)

        const ring = new THREE.Mesh(new THREE.RingGeometry(Math.SQRT2 * centerSize / 2, 30, 4, 1), new THREE.MeshPhongMaterial(_.extend(materialParams, {
            alphaMap: this.generateGroundAlphaTexture(),
            transparent: true
        })))
        ring.rotateZ(Math.PI/4)
        ring.castShadow = false
        ring.receiveShadow = false
        ground.add(ring)

        ground.rotation.x = -Math.PI/2
        ground.position.y = -0.0001
        return ground
    }
}