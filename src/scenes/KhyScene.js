import Scene from './Scene'
import THREE from 'three'
import RenderLoopEvent from '../lib/RenderLoopEvent'
import Listener from '../lib/Listener'

/**
 * @extends Scene
 */
export default class KhyScene extends Scene {

    constructor() {
        super()

        var ambient = new THREE.AmbientLight(0x282828) //0x101030
        this.scene.add(ambient)
        
        let spotLight = this.spotLight = new THREE.SpotLight(0xaaaaff, 1)
        spotLight.name = 'Spot Light'
        spotLight.position.set(5, 2, -25)
        spotLight.angle = Math.PI/16
        spotLight.castShadow = true
        //spotLight.shadowBias = 0.000001
        spotLight.shadow.camera.near = 5
        spotLight.shadow.camera.far = 15
        spotLight.shadow.mapSize.width = 1024
        spotLight.shadow.mapSize.height = 1024
        this.scene.add(spotLight)

        // this.scene.add(this.spotLightHelper2 = new THREE.SpotLightHelper(spotLight))

        let dirLight = this.dirLight = new THREE.SpotLight(0xff9999, 0.7)
        dirLight.name = 'Spot Light'
        dirLight.angle = Math.PI/16
        dirLight.position.set(1, -1, 5)
        dirLight.castShadow = true
        //spotLight.shadowBias = 0.000001
        dirLight.shadow.camera.near = 5
        dirLight.shadow.camera.far = 15
        dirLight.shadow.mapSize.width = 1024
        dirLight.shadow.mapSize.height = 1024
        this.scene.add(dirLight)

        // this.scene.add(this.spotLightHelper = new THREE.SpotLightHelper(dirLight))

        this.addListener(new Listener(RenderLoopEvent, (event) => {
            const now = event.now

            this.dirLight.position.set(3*Math.sin(now/5000), 1, 3*Math.cos(now/5000))
            this.spotLight.position.set(2*Math.cos(now/3000), 2, 2*Math.sin(now/3000))
            // this.spotLightHelper.update()
            // this.spotLightHelper2.update()
        }))
    }
}