import Scene from './Scene'
import KhyScene from './KhyScene'
import DaylightScene from './DaylightScene'

/**
 * @extends Scene
 * @property {Scene} activeScene
 * @property {Set<Scene>} scenes
 */
export default class SceneManager extends Scene {

    constructor() {
        super()

        this.activeScene = null
        this.scenes = new Set()

        this.addScene(new DaylightScene())
        this.addScene(new KhyScene())
        this.setScene(DaylightScene)
    }

    setScene(scene) {
        if (typeof scene === 'function') {
            const sceneInstance = [...this.scenes].find((e) => scene.prototype.isPrototypeOf(e))
            if (!scene) {
                throw new Error('Could not find ' + scene + ' in scene list')
            }
            scene = sceneInstance
        }
        if (this.activeScene) {
            this.remove(this.activeScene)
        }
        this.add(scene)
        this.activeScene = scene
    }

    /**
     * @param {Scene} scene
     */
    addScene(scene) {
        this.scenes.add(scene)
    }

    nextScene() {
        const index = [...this.scenes].indexOf(this.activeScene);
        const nextIndex = (index + 1) % this.scenes.size
        this.setScene([...this.scenes][nextIndex])
    }

    onAttach() {
    }
}