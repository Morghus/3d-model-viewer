import THREE from 'three'

/**
 * @extends THREE.Object3D
 */
export default class SkySphere extends THREE.Object3D {
    /**
     * @param {THREE.WebGLRenderer} renderer
     */
    constructor(renderer) {
        super()
        const cubeMap = this.createCubeMap(renderer);

        var shader = THREE.ShaderLib['cube'];
        shader.uniforms['tCube'].value = cubeMap;

        var skyBoxMaterial = new THREE.ShaderMaterial( {
            fragmentShader: shader.fragmentShader,
            vertexShader: shader.vertexShader,
            uniforms: shader.uniforms,
            depthWrite: false,
            side: THREE.BackSide
        });
        
        this.add(new THREE.Mesh(new THREE.BoxGeometry(100, 100, 100), skyBoxMaterial))
    }

    /**
     * @param {THREE.WebGLRenderer} renderer
     * @returns {THREE.WebGLRenderTargetCube}
     */
    createCubeMap(renderer) {
        const uniforms = {
            topColor: {type: "c", value: new THREE.Color(0x000000)},
            bottomColor: {type: "c", value: new THREE.Color(0x666666)},
            offset: {type: "f", value: 99},
            exponent: {type: "f", value: 0.7}
        }

        const vertexShader = require('./SkySphere.vert')
        const fragmentShader = require('./SkySphere.frag')

        const skyScene = new THREE.Scene();

        const sky = new THREE.Mesh(new THREE.SphereGeometry(50, 32, 15), new THREE.ShaderMaterial({
            uniforms,
            vertexShader,
            fragmentShader,
            side: THREE.BackSide
        }))
        skyScene.add(sky)

        const skyCamera = new THREE.CubeCamera(0.1, 1000, 2048)
        skyScene.add(skyCamera)

        skyCamera.updateCubeMap(renderer, skyScene)
        return skyCamera.renderTarget;
    }
}