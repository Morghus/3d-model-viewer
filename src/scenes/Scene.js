import Behavior from '../lib/Behavior'
import THREE from 'three'

/**
 * @extends Behavior
 * @property {ModelViewer} component 
 * @property {THREE.Object3D} scene
 * @property {Set} lights
 */
export default class Scene extends Behavior {

    constructor() {
        super()
        this.scene = new THREE.Object3D()
    }

    onAttach() {
        this.component.scene.add(this.scene)
    }

    onDetach() {
        this.component.scene.remove(this.scene)
    }
}