import CANNON from 'cannon'
import Behavior from "../lib/Behavior";
import RenderLoopEvent from "../lib/RenderLoopEvent";
import Listener from "../lib/Listener";

/**
 * @property {CANNON.World} world
 */
export default class PhysicsBehavior extends Behavior {
    constructor() {
        super()

        this.world = this.createWorld();
        
        this.addListener(new Listener(RenderLoopEvent, () => {
            this.world.step(1/60)
        }))
    }

    createWorld() {
        const world = new CANNON.World()
        world.gravity.set(0,0,0)
        world.broadphase = new CANNON.NaiveBroadphase()
        world.solver.iterations = 10
        return world
    }
    
    onAttach(component) {
        
    }
    
    onDetach(component) {
        
    }
}