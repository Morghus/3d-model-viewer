import Behavior from '../lib/Behavior'
import SideBySide3DBehavior from './SideBySide3DBehavior'
import THREE from 'three'
import Mousetrap from "mousetrap";
import Listener from "../lib/Listener";
import LowFpsEvent from "../lib/LowFpsEvent";
import * as screenfull from "screenfull";
import SceneManager from "../scenes/SceneManager";

/**
 * @property {SideBySide3DBehavior} sbs
 * @extends Behavior
 */
export default class KeyboardControls extends Behavior {
    constructor() {
        super()
        this.sbs = new SideBySide3DBehavior()
        this.addListener(new Listener(LowFpsEvent, (event) => {
            console.log('Disabling shadows because FPS are low')
            this.shadowsEnabled = false
        }))

        this.lastTouch = null
    }

    /**
     * @param {TouchEvent} event
     */
    onTouch(event) {
        const now = Date.now();
        if (this.lastTouch !== null && now - this.lastTouch < 500) {
            screenfull.toggle()
        }
        this.lastTouch = now
    }

    onAttach() {
        super.onAttach()
        Mousetrap.bind('3 d', () => this.toggle3D())
        Mousetrap.bind('s', () => this.toggleShadows())
        Mousetrap.bind('n', () => this.nextScene())

        this.boundOnTouch = this.onTouch.bind(this)
        document.body.addEventListener('touchend', this.boundOnTouch)
    }


    toggle3D() {
        if (this.sbs.attached()) {
            this.component.removeBehavior(this.sbs)
        } else {
            this.component.addBehavior(this.sbs)
        }
    }

    get shadowsEnabled() {
        return this.component.renderer.shadowMap.enabled
    }
    set shadowsEnabled(enabled) {
        this.component.renderer.shadowMap.enabled = enabled
        this.component.scene.traverse((object) => {
            if (object instanceof THREE.SpotLight || object instanceof THREE.DirectionalLight) {
                object.castShadow = enabled
            }
        })
    }
    toggleShadows() {
        this.shadowsEnabled = !this.shadowsEnabled
    }
    
    nextScene() {
        /** @type {SceneManager} */
        const sceneManager = this.component.behavior(SceneManager);
        if (sceneManager) {
            sceneManager.nextScene()
        }
    }

    onDetach() {
        Mousetrap.unbind('3 d')
        Mousetrap.unbind('s')
        Mousetrap.unbind('n')

        document.body.removeEventListener('touchend', this.boundOnTouch)
    }
}