import RenderLoopEvent from '../lib/RenderLoopEvent'
import WindowResizeEvent from '../lib/WindowResizeEvent'
import THREE from 'three'
import 'imports-loader?THREE=three!three/examples/js/controls/TrackballControls'
import Behavior from '../lib/Behavior'
import Listener from '../lib/Listener'
import engine from '../engine'

/** @property {engine} component */
export default class TrackballCameraBehavior extends Behavior {
    constructor() {
        super({expectedType: engine})
        this.addListener(new Listener(RenderLoopEvent, () => {
            this.controls.update()
        }))
        this.addListener(new Listener(WindowResizeEvent, () => {
            this.controls.handleResize()
        }))
    }
    onAttach() {
        let controls = this.controls = new THREE.TrackballControls(this.component.camera);

        controls.rotateSpeed = 2;
        controls.zoomSpeed = 1.2;
        controls.panSpeed = 0.01;

        controls.noZoom = false;
        controls.noPan = false;

        controls.minDistance = 0.5;
        controls.maxDistance = 490;

        controls.staticMoving = false;
        controls.dynamicDampingFactor = 0.1;

        controls.keys = [65, 83, 68];
    }

    onDetach() {
        this.controls.dispose()
        this.controls = null;
    }
}
