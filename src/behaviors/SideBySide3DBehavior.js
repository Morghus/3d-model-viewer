import Behavior from '../lib/Behavior'
import RenderLoopEvent from '../lib/RenderLoopEvent'
import Listener from '../lib/Listener'
import THREE from 'three'
import StereoEffect from '../utils/THREE.StereoEffect'

/**
 * @extends Behavior
 */
export default class SideBySide3DBehavior extends Behavior {
    constructor() {
        super()
        this.addListener(new Listener(RenderLoopEvent, (event) => {
            let camera = event.source.camera;
            camera.focalLength = camera.position.length()
        }));
    }

    onAttach() {
        let {width, height} = this.component.getSize()

        let effect = new THREE.StereoEffect(this.component.renderer);
        effect.eyeSeparation = 800;
        effect.setSize(width, height);

        this.component.setAlternateRenderer(effect)

        this.oldFov = this.component.camera.fov
        this.component.camera.fov = 23.5
        this.component.camera.updateProjectionMatrix()
        SideBySide3DBehavior.changeDistance(this.component.camera.position, this.oldFov, this.component.camera.fov)

        this.component.onWindowResize()
    }

    static changeDistance(position, oldFov, newFov) {
        let oldLength = position.length();
        let newLength = oldLength / Math.tan(newFov / 2 * (Math.PI / 180)) * Math.tan(oldFov / 2 * (Math.PI / 180))
        position.multiplyScalar(newLength / oldLength)
    }

    onDetach() {
        this.component.setAlternateRenderer(null)
        SideBySide3DBehavior.changeDistance(this.component.camera.position, this.component.camera.fov, this.oldFov)
        this.component.camera.fov = this.oldFov
        this.component.onWindowResize()
    }
}