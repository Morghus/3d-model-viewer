import Behavior from '../lib/Behavior'
import RenderLoopEvent from '../lib/RenderLoopEvent'
import Listener from '../lib/Listener'
import THREE from 'three'
import 'imports-loader?THREE=three!three/examples/js/controls/OrbitControls'

/**
 * @property {ModelViewer} component
 * @property {THREE.OrbitControls} controls
 */
export default class OrbitCameraBehavior extends Behavior {

    constructor() {
        super()
        this.caster = new THREE.Raycaster()
        this.down = new THREE.Vector3(0, -1, 0).normalize()
        this.lastPos = null
        this.addListener(new Listener(RenderLoopEvent, /** @param {RenderLoopEvent} event */ (event) => {
            this.controls.autoRotateSpeed = event.delta * 0.01 * Math.sign(this.controls.autoRotateSpeed)
            this.controls.dampingFactor = event.delta * 0.005

            this.controls.update()

            this.collisionDetection()
        }));
    }

    collisionDetection() {
        const pos = this.component.camera.position.clone()

        this.caster.set(pos, this.down)
        const collisionRadius = 0.05;
        const objects = [...this.component.collisionObjects];
        let intersections = this.caster.intersectObjects(objects, true).filter((intersection) => intersection.distance < collisionRadius)
        if (this.lastPos !== null) {
            const lastPosDiff = pos.clone().sub(this.lastPos);
            const distance = lastPosDiff.length();
            if (distance > 0) {
                this.caster.set(this.lastPos, lastPosDiff.normalize())
                const lastDiffIntersections = this.caster.intersectObjects(objects, true)
                    .filter((intersection) => intersection.distance <= distance);
                intersections = intersections.concat(lastDiffIntersections)
                intersections.sort((a, b) => a.distance - b.distance)
            }
        }

        if (intersections.length > 0) {
            this.component.camera.position.y = intersections[0].point.y + collisionRadius
            this.component.camera.lookAt(this.controls.target)
        } else {
            this.lastPos = pos
        }
    };

    onAttach() {
        this.controls = this.createControls();

    }

    createControls() {
        const controls = new THREE.OrbitControls(this.component.camera)
        controls.enableDamping = true
        controls.dampingFactor = 0.1
        controls.enableZoom = true
        controls.enablePan = false
        controls.rotateSpeed = 0.08
        controls.zoomSpeed = 0.5
        controls.minDistance = 0.3
        controls.maxDistance = 3
        controls.autoRotateSpeed = 0.15

        this.makeAutorotation(controls)

        return controls
    };

    /**
     * @param {THREE.OrbitControls} controls
     */
    makeAutorotation(controls) {
        controls.addEventListener('start', () => {
            controls.autoRotate = false
        })

        controls.addEventListener('end', () => {
            const startAutorotate = Math.abs(arcDiff) > 0.005
            controls.autoRotate = startAutorotate
            if (startAutorotate) {
                controls.autoRotateSpeed = Math.abs(controls.autoRotateSpeed) * (arcDiff > 0 ? -1 : 1)
            }
        })

        let lastAngle = null
        let arcDiff = null
        controls.addEventListener('change', () => {
            const angle = controls.getAzimuthalAngle();
            const diff = angle - lastAngle;
            arcDiff = Math.atan2(Math.sin(diff), Math.cos(diff))
            lastAngle = angle
        })
    }

    onDetach() {
        this.controls.dispose()
        this.controls = null
    }
}