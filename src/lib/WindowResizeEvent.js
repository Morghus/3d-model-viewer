import BaseEvent from './BaseEvent'

/** @extends Behavior */
export default class WindowResizeEvent extends BaseEvent {
    /**
     *
     * @param {Component} source
     * @param {object} options
     * @param {number} options.width
     * @param {number} options.height
     */
    constructor(source, options) {
        super(source)
        options = options || {}
        this.width = options.width
        this.height = options.height
    }
}