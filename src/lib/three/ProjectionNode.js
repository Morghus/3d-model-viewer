import THREE from 'three'
import './NodeMaterials'

/**
 * @param {THREE.InputNode} texture
 * @param {THREE.InputNode} matrix
 * @constructor
 */
const ProjectionNode = function(texture, matrix) {
    THREE.InputNode.call( this, 'v4' );
    this.texture = texture
    this.matrix = matrix
    ProjectionNode.index = (ProjectionNode.index || 0) + 1
    this.i = ProjectionNode.index
}
ProjectionNode.prototype = Object.create( THREE.InputNode.prototype )
ProjectionNode.prototype.constructor = ProjectionNode

ProjectionNode.prototype.generate = function( builder, output, uuid) {

    var material = builder.material;

    uuid = builder.getUuid( uuid || this.uuid );

    var data = material.getDataNode( uuid );

    const varName = 'projectionCoord' + this.i

    if ( builder.isShader( 'vertex' ) ) {

        if ( ! data.vertex ) {
            data.vertex = this.matrix.build( builder, 'v2' );
            material.vertexPars += 'varying vec4 ' + varName + ';' + "\n"
            material.vertexCode += varName + ' = ' + data.vertex + ' * modelMatrix * vec4( position, 1.0 );'
        }

        return builder.format( data.vertex, 'm4', output );

    }
    else {
        if (! data.fragment ) {
            material.fragmentPars += 'varying vec4 ' + varName + ';' + "\n"
            data.fragment = material.getFragmentUniform( this.texture.value, 't')
        }

        const code = '(' + varName + '.q > 0.0 ? texture2DProj(' + data.fragment.name + ', ' + varName + ') : vec4(1.0))'

        return builder.format(code, this.type, output)
    }
}

export default ProjectionNode
