/**
 * @property {T} source
 * @template T
 */
export default class BaseEvent {
    /**
     * @param {T} source
     */
    constructor(source) {
        this.source = source
    }
}