import Component from "./Component";

/**
 * @property {Set} children
 * @property {Component} component
 * @property {Behavior} parent
 * @property {function} expectedType
 */
export default class Behavior {
    constructor() {
        this.component = null
        this.parent = null
        this.children = new Set()
        this.eventListeners = new Set()
    }

    /** @param {Component} component */
    attach(component) {
        if (this.component !== null) {
            throw new Error('Behavior ' + this.constructor.name + ' is already attached');
        }
        this.component = component
        this.onAttach()
        this.children.forEach((child) => component.addBehavior(child))
        this.eventListeners.forEach((listener) => component.addListener(listener))
    }

    detach() {
        this.onDetach()
        if (this.component === null) {
            throw new Error('Behavior ' + this.constructor.name + ' is already detached');
        }
        this.children.forEach((child) => this.component.removeBehavior(child))
        this.eventListeners.forEach((listener) => this.component.removeListener(listener))
        this.component = null;
    }

    onAttach() {
    }

    onDetach() {
    }

    attached() {
        return this.component !== null
    }

    addListener(listener) {
        this.eventListeners.add(listener)
        if (this.attached()) {
            this.component.addListener(listener)
        }
    }

    removeListener(listener) {
        this.eventListeners.delete(listener)
        if (this.attached()) {
            this.component.removeListener(listener)
        }
    }

    /** @param {Behavior} behavior */
    add(behavior) {
        if (behavior.component !== this.component && this.attached()) {
            // child behavior needs to be added
            this.component.addBehavior(behavior)
        }
        if (behavior.component !== this.component) {
            throw new Error('Child behavior is already attached to a different component')
        }

        this.children.add(behavior)
        behavior.parent = this
    }

    /** @param {Behavior} behavior */
    remove(behavior) {
        if (behavior.component === this.component && this.attached()) {
            this.component.removeBehavior(behavior);
        }
        this.children.delete(behavior)
    }
}
