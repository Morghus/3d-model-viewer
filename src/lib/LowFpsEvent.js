import BaseEvent from './BaseEvent'


export default class LowFpsEvent extends BaseEvent {
    constructor(source, fps) {
        super(source)
        this.fps = fps
    }
}