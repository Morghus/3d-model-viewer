import BaseEvent from './BaseEvent'

/**
 * @extends BaseEvent<T>
 * @property {number} delta
 * @property {number} now
 * @template T
 */
export default class RenderLoopEvent extends BaseEvent {
    /**
     * @param {T} source
     * @param {Object} options
     * @param {number} options.delta
     * @param {number} options.now
     */
    constructor(source, options) {
        options = options || {}
        super(source)
        this.delta = options.delta
        this.now = options.now
    }
}