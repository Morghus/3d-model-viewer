/**
 * @callback eventCallback
 * @param {T} event
 */

/**
 * @property {T} eventType
 * @property {eventCallback} callback
 * @template T
 */
export default class Listener {
    /**
     * @param {T} eventType
     * @param {eventCallback} callback
     */
    constructor(eventType, callback) {
        this.eventType = eventType
        this.callback = callback
    }

    /**
     * @param {T} event
     */
    event(event) {
        this.callback(event)
    }
}