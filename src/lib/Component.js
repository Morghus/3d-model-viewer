import Behavior from './Behavior';
import BaseEvent from './BaseEvent'

/**
 * @property {Set<Behavior>} _behaviors
 * @property {Set<Listener>} _eventListeners
 */
export default class Component {

    constructor() {
        this._behaviors = new Set()
        this._eventListeners = new Set()
    }

    set behaviors(behaviors) {
        this._behaviors = behaviors
    }
    get behaviors() {
        return this._behaviors
    }

    /**
     * @param {T} type
     * @returns {T}
     * @template T
     */
    behavior(type) {
        return [...this._behaviors].find((behavior) => behavior instanceof type)
    }
    addBehavior(behavior) {
        behavior.attach(this)
        this.behaviors.add(behavior)
    }

    removeBehavior(behavior) {
        this.behaviors.delete(behavior)
        behavior.detach(this)
    }

    /** @param {BaseEvent} event */
    trigger(event) {
        event.source = this;
        [...this._eventListeners]
            .filter((listener) => event instanceof listener.eventType)
            .forEach((listener) => listener.event(event))
    }

    addListener(listener) {
        this._eventListeners.add(listener)
    }

    removeListener(listener) {
        this._eventListeners.add(listener)
    }
}