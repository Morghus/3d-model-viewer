import engine from './engine'
import ModelLoader from './utils/ModelLoader'
import * as THREE from "three";
import OrbitCameraBehavior from "./behaviors/OrbitCameraBehavior";

/**
 * @extends engine
 * @property {PhysicsBehavior} physics
 * @property {THREE.Mesh} model
 * @property {Promise<THREE.Object3D>} promise
 * @property {function} resolve
 * @property {Set<THREE.Object3D>} collisionObjects
 */
export default class ModelViewer extends engine {
    constructor() {
        super()
        this.model = null
        this.promise = new Promise((resolve) => {
            this.resolve = resolve
        })
        this.collisionObjects = new Set()
    }

    loadModel(path, progress) {
        return new ModelLoader()
            .load(path, progress)
            .then((object) => {
                this.model = object
                this.scene.add(object)
                this.resolve(object)
                return object
            })
    }

    /**
     * @returns {Promise<THREE.Object3D>}
     */
    getModel() {
        return this.promise
    }
}