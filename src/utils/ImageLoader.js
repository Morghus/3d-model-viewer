import THREE from 'three'

export default class ImageLoader {
    constructor(manager) {
        this.manager = manager
    }

    load(url, load, progress, error) {
        var loader = new THREE.XHRLoader(this.manager);
        loader.setPath(this.path);
        loader.setResponseType("blob");
        loader.load(url, (buf) => {
            var img = document.createElement('img')
            img.onload = () => load(img)
            img.onerror = () => error(img)
            img.src = window.URL.createObjectURL(buf)
        }, progress, error);
    }
}