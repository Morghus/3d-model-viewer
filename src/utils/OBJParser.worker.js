import OBJParser from './OBJParser'

onmessage = function (message) {
    OBJParser(message.data).then(postMessage)
}