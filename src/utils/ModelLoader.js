"use strict"
import THREE from 'three'
import _ from 'lodash'
import OBJLoader from './OBJLoader'
import ImageLoader from './ImageLoader'

export class XHRError extends Error {
    constructor(msg, xhr) {
        super(msg)
        this.xhr = xhr
    }
}

export default class ModelLoader {
    /**
     * @param {string} path
     * @param {function} [progress]
     * @param {THREE.LoadingManager} [manager]
     */
    constructor(manager) {
        this.progressRegistry = {}
        this.manager = manager
    }

    addProgressFor(filename) {
        let entry = this.progressRegistry[filename] = {}
        return (xhr) => {
            entry.loaded = xhr.loaded
            entry.total = xhr.total
            this.triggerProgressEvent()
        }
    }

    /**
     * Triggers the onProgress event if all entries in have a 'total' value.
     */
    triggerProgressEvent() {
        if (typeof this.progress === 'function'
                && _.every(this.progressRegistry, (element) => typeof element.total !== 'undefined')) {
            let sum = _.reduce(this.progressRegistry, (sum, e) => {
                return {
                    loaded: sum.loaded + e.loaded,
                    total: sum.total + e.total
                }
            })
            this.progress(sum)
        }
    }

    loadTexture(filename, totalSize) {
        return this.loadResource(filename, totalSize, ImageLoader)
    }
    loadObject(filename, totalSize) {
        return this.loadResource(filename, totalSize, OBJLoader)
    }
    loadResource(filename, totalSize, loader) {
        let progress = this.addProgressFor(filename)
        return new Promise((resolve, reject) => {
            new loader(this.manager).load(filename, resolve, (xhr) => {
                progress({
                    loaded: xhr.loaded,
                    total: totalSize || xhr.total
                })
            }, (xhr) => reject(new XHRError('Could not load "' + filename + '"', xhr)));
        })
    }

    /**
     * @returns {Promise<THREE.Object3D>}
     */
    load(path, progress) {
        return new Promise((resolve, reject) => {
            this.progress = progress

            let transformImageToTexture = (image) => {
                let texture = new THREE.Texture(image)
                texture.needsUpdate = true
                return texture
            }

            let jsonRequest = new THREE.XHRLoader(this.manager);
            jsonRequest.load(path + '/model.json', (modelJsonString) => {
                const modelJson = _.extend({properties: {}}, JSON.parse(modelJsonString))
                if (!modelJson) {
                    reject(new Error('Could not parse Model JSON'))
                    return
                }

                if (!modelJson.model) {
                    reject(new Error('Model JSON did not specify a model'))
                    return
                }

                let textures = null, bumpmaps = null
                if (modelJson.textures) {
                    textures = modelJson.textures.map((texture) => this.loadTexture(path + '/' + texture)
                        .then(transformImageToTexture)
                        .then((map) => {
                            map.name = texture
                            return map
                        }));
                }
                if (modelJson.bumpmaps) {
                    bumpmaps = modelJson.bumpmaps.map((bumpmap) => this.loadTexture(path + '/' + bumpmap)
                        .then(transformImageToTexture)
                        .then((map) => {
                            map.format = THREE.LuminanceFormat
                            map.anisotropy = 4
                            map.needsUpdate = true
                            map.name = bumpmap
                            return map
                        }))
                }
                let object = this.loadObject(path + '/' + modelJson.model, modelJson.modelSize)
                    .then(this.centerObject)

                Promise.all([].concat([object], textures, bumpmaps)).then(([object, ...maps]) => {
                    let textures = _.keyBy(maps, 'name')

                    object.traverse((child) => this.traverseObject(child, modelJson, textures));
                    resolve(object)
                }).catch(reject)
            }, undefined, (xhr) => reject(new XHRError('Could not load "model.json"', xhr)))
        })
    }

    centerObject(object) {
        const box = new THREE.Box3()
        box.setFromObject(object)
        const size = box.size()
        const scale = 1 / Math.max(size.x, size.y, size.z)
        object.scale.multiplyScalar(scale)

        box.setFromObject(object)

        box.center(object.position)
        object.position.multiplyScalar(-1);

        const parent = new THREE.Object3D()
        parent.add(object)

        return parent
    }

    traverseObject(child, modelJson, textures) {
        if (child instanceof THREE.Mesh) {
            child.material = child.material.clone()

            let properties = _.extend({}, modelJson.properties['*'] || {}, modelJson.properties[child.name])
            for (let prop in properties) {
                let value = properties[prop]
                if (typeof value === 'string') {
                    if (value.charAt(0) == '#') {
                        value = new THREE.Color(value)
                    } else if (textures[value] instanceof THREE.Texture) {
                        value = textures[value]
                    }
                }
                let element = child
                prop.split('.').forEach((part, i, arr) => {
                    if (i === arr.length - 1) {
                        element[part] = value
                        console.log(child.name, prop, value)
                    } else {
                        element = element[part]
                    }
                })
            }

            child.material.needsUpdate = true
        }
    }
}