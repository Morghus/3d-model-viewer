import THREE from 'three'
//import OBJParserWorker from './OBJParser.worker'
import OBJParser from './OBJParser'

export default class OBJLoader{
    constructor(manager) {
        this.manager = ( manager !== undefined ) ? manager : THREE.DefaultLoadingManager;

        this.materials = null
    }

    load(url, onLoad, onProgress, onError) {
        var loader = new THREE.XHRLoader(this.manager);
        loader.setPath(this.path);
        loader.setResponseType("arraybuffer");
        loader.load(url, (buf) => {
            this.parse(buf, onLoad);
        }, onProgress, onError);
    }

    setPath (value) {
        this.path = value;
    }

    parseObjects(objects) {
        var container = new THREE.Group();

        for (var i = 0, l = objects.length; i < l; i++) {

            let object = objects[i];
            var geometry = object.geometry;

            var buffergeometry = new THREE.BufferGeometry();

            buffergeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(geometry.vertices), 3));
            buffergeometry.addAttribute('color', new THREE.BufferAttribute(new Float32Array(geometry.colors), 3));

            if (geometry.normals.length > 0) {
                buffergeometry.addAttribute('normal', new THREE.BufferAttribute(new Float32Array(geometry.normals), 3));
            } else {
                buffergeometry.computeVertexNormals();
            }

            if (geometry.uvs.length > 0) {
                const uvs = new THREE.BufferAttribute(new Float32Array(geometry.uvs), 2);
                buffergeometry.addAttribute('uv', uvs);
                buffergeometry.addAttribute('uv2', uvs);
            }

            var material;

            if (this.materials !== null) {
                material = this.materials.create(object.material.name);
            }

            if (!material) {
                if (object.material.name == 'Plane') {
                    material = new THREE.MeshBasicMaterial();
                } else {
                    material = new THREE.MeshPhongMaterial();
                }
                material.name = object.material.name;
            }

            material.shading = THREE.SmoothShading

            var mesh = new THREE.Mesh(buffergeometry, material);
            mesh.name = object.name;
            mesh.castShadow = true;
            mesh.receiveShadow = true;

            container.add(mesh);
        }

        return container
    }

    parse (buf, onLoad) {

        console.time('OBJLoader');

        //let worker = new OBJParserWorker()
        //worker.onmessage = (message) => {
        //    let objects = message.data
        //    let container = this.parseObjects(objects)
        //    onLoad(container)
        //    console.timeEnd('OBJLoader');
        //}
        //worker.postMessage(buf)

        OBJParser(buf).then((result) => {
            const parsed = this.parseObjects(result)
            console.timeEnd('OBJLoader');
            onLoad(parsed)
        })
    }
}
