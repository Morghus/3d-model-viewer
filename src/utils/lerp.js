let lastTime;
let timeDiff;

let lerp = function lerp(source, target) {
    var diff = (target - source)
    if (Math.abs(diff / target) < 0.0001) {
        return target
    }
    return source + (diff * (timeDiff / 1000));
};
lerp.tick = function tick() {
    let now = Date.now();
    timeDiff = now - lastTime;
    lastTime = now;
}
export default lerp
