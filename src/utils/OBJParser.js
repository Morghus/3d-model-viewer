
const TextDecoderPromise = new Promise((resolve) => {
    "use strict";
    if (!window.TextDecoder) {
        require.ensure(['text-encoding'], (require) => {
            resolve(require('text-encoding').TextDecoder)
        });
    } else {
        resolve(window.TextDecoder)
    }
})


export default function (buf, onProgress) {
    onProgress = onProgress || (() => {});
    return new Promise((resolve) => {
        "use strict";

        TextDecoderPromise.then((TextDecoderShim) => {
            let decoder = new TextDecoderShim("utf-8");
            let dataView;

            let objects = [];
            let object;
            let foundObjects = false;
            let vertices = [];
            let normals = [];
            let uvs = [];
            let colors = [];

            function addObject(name) {

                let geometry = {
                    vertices: [],
                    normals: [],
                    uvs: [],
                    colors: []
                };

                let material = {
                    name: '',
                    smooth: true
                };

                object = {
                    name: name,
                    geometry: geometry,
                    material: material
                };

                objects.push(object);

            }

            function parseVertexIndex(value) {

                let index = parseInt(value);

                return ( index >= 0 ? index - 1 : index + vertices.length / 3 ) * 3;

            }

            function parseNormalIndex(value) {

                let index = parseInt(value);

                return ( index >= 0 ? index - 1 : index + normals.length / 3 ) * 3;

            }

            function parseUVIndex(value) {

                let index = parseInt(value);

                return ( index >= 0 ? index - 1 : index + uvs.length / 2 ) * 2;

            }

            function addVertex(a, b, c) {

                object.geometry.vertices.push(
                    vertices[a], vertices[a + 1], vertices[a + 2],
                    vertices[b], vertices[b + 1], vertices[b + 2],
                    vertices[c], vertices[c + 1], vertices[c + 2]
                );

                object.geometry.colors.push(
                    colors[a], colors[a + 1], colors[a + 2],
                    colors[b], colors[b + 1], colors[b + 2],
                    colors[c], colors[c + 1], colors[c + 2]
                );

            }

            function addNormal(a, b, c) {

                object.geometry.normals.push(
                    normals[a], normals[a + 1], normals[a + 2],
                    normals[b], normals[b + 1], normals[b + 2],
                    normals[c], normals[c + 1], normals[c + 2]
                );

            }

            function addUV(a, b, c) {

                object.geometry.uvs.push(
                    uvs[a], uvs[a + 1],
                    uvs[b], uvs[b + 1],
                    uvs[c], uvs[c + 1]
                );

            }

            function addFace(a, b, c, d, ua, ub, uc, ud, na, nb, nc, nd) {

                let ia = parseVertexIndex(a);
                let ib = parseVertexIndex(b);
                let ic = parseVertexIndex(c);
                let id;

                if (d === undefined) {

                    addVertex(ia, ib, ic);

                } else {

                    id = parseVertexIndex(d);

                    addVertex(ia, ib, id);
                    addVertex(ib, ic, id);

                }

                if (ua !== undefined) {

                    ia = parseUVIndex(ua);
                    ib = parseUVIndex(ub);
                    ic = parseUVIndex(uc);

                    if (d === undefined) {

                        addUV(ia, ib, ic);

                    } else {

                        id = parseUVIndex(ud);

                        addUV(ia, ib, id);
                        addUV(ib, ic, id);

                    }

                }

                if (na !== undefined) {

                    ia = parseNormalIndex(na);
                    ib = parseNormalIndex(nb);
                    ic = parseNormalIndex(nc);

                    if (d === undefined) {

                        addNormal(ia, ib, ic);

                    } else {

                        id = parseNormalIndex(nd);

                        addNormal(ia, ib, id);
                        addNormal(ib, ic, id);

                    }

                }

            }

            addObject('');

            // v float float float
            let vertex_pattern = /^v\s+([\d|\.|\+|\-|e|E]+)\s+([\d|\.|\+|\-|e|E]+)\s+([\d|\.|\+|\-|e|E]+)/;

            // vn float float float
            let normal_pattern = /^vn\s+([\d|\.|\+|\-|e|E]+)\s+([\d|\.|\+|\-|e|E]+)\s+([\d|\.|\+|\-|e|E]+)/;

            // vt float float
            let uv_pattern = /^vt\s+([\d|\.|\+|\-|e|E]+)\s+([\d|\.|\+|\-|e|E]+)/;

            // f vertex vertex vertex ...
            let face_pattern1 = /^f\s+(-?\d+)\s+(-?\d+)\s+(-?\d+)(?:\s+(-?\d+))?/;

            // f vertex/uv vertex/uv vertex/uv ...
            let face_pattern2 = /^f\s+((-?\d+)\/(-?\d+))\s+((-?\d+)\/(-?\d+))\s+((-?\d+)\/(-?\d+))(?:\s+((-?\d+)\/(-?\d+)))?/;

            // f vertex/uv/normal vertex/uv/normal vertex/uv/normal ...
            let face_pattern3 = /^f\s+((-?\d+)\/(-?\d+)\/(-?\d+))\s+((-?\d+)\/(-?\d+)\/(-?\d+))\s+((-?\d+)\/(-?\d+)\/(-?\d+))(?:\s+((-?\d+)\/(-?\d+)\/(-?\d+)))?/;

            // f vertex//normal vertex//normal vertex//normal ...
            let face_pattern4 = /^f\s+((-?\d+)\/\/(-?\d+))\s+((-?\d+)\/\/(-?\d+))\s+((-?\d+)\/\/(-?\d+))(?:\s+((-?\d+)\/\/(-?\d+)))?/;

            // #MRGB
            let mrgb_pattern = /^#MRGB (.+)/;

            let object_pattern = /^[og]\s+(.+)/;

            let smoothing_pattern = /^s\s+(\d+|on|off)/;

            function parseLine(line) {
                let result;

                if (line.length === 0 || line.charCodeAt(0) === 0) {

                    return;

                } else if (( result = vertex_pattern.exec(line) ) !== null) {

                    // ["v 1.0 2.0 3.0", "1.0", "2.0", "3.0"]

                    vertices.push(
                        parseFloat(result[1]),
                        parseFloat(result[2]),
                        parseFloat(result[3])
                    );

                } else if (( result = normal_pattern.exec(line) ) !== null) {

                    // ["vn 1.0 2.0 3.0", "1.0", "2.0", "3.0"]

                    normals.push(
                        parseFloat(result[1]),
                        parseFloat(result[2]),
                        parseFloat(result[3])
                    );

                } else if (( result = uv_pattern.exec(line) ) !== null) {

                    // ["vt 0.1 0.2", "0.1", "0.2"]

                    uvs.push(
                        parseFloat(result[1]),
                        parseFloat(result[2])
                    );


                } else if (( result = mrgb_pattern.exec(line) ) !== null) {

                    let value = result[1];
                    for (let j = 0; j < value.length; j += 8) {
                        let r = parseInt(value.substr(j + 2, 2), 16) / 255;
                        let g = parseInt(value.substr(j + 4, 2), 16) / 255;
                        let b = parseInt(value.substr(j + 6, 2), 16) / 255;
                        colors.push(r, g, b);
                    }

                } else if (( result = face_pattern1.exec(line) ) !== null) {

                    // ["f 1 2 3", "1", "2", "3", undefined]

                    addFace(
                        result[1], result[2], result[3], result[4]
                    );

                } else if (( result = face_pattern2.exec(line) ) !== null) {

                    // ["f 1/1 2/2 3/3", " 1/1", "1", "1", " 2/2", "2", "2", " 3/3", "3", "3", undefined, undefined, undefined]

                    addFace(
                        result[2], result[5], result[8], result[11],
                        result[3], result[6], result[9], result[12]
                    );

                } else if (( result = face_pattern3.exec(line) ) !== null) {

                    // ["f 1/1/1 2/2/2 3/3/3", " 1/1/1", "1", "1", "1", " 2/2/2", "2", "2", "2", " 3/3/3", "3", "3", "3", undefined, undefined, undefined, undefined]

                    addFace(
                        result[2], result[6], result[10], result[14],
                        result[3], result[7], result[11], result[15],
                        result[4], result[8], result[12], result[16]
                    );

                } else if (( result = face_pattern4.exec(line) ) !== null) {

                    // ["f 1//1 2//2 3//3", " 1//1", "1", "1", " 2//2", "2", "2", " 3//3", "3", "3", undefined, undefined, undefined]

                    addFace(
                        result[2], result[5], result[8], result[11],
                        undefined, undefined, undefined, undefined,
                        result[3], result[6], result[9], result[12]
                    );

                } else if (( result = object_pattern.exec(line) ) !== null) {

                    // o object_name
                    // or
                    // g group_name

                    let name = result[1].trim();

                    if (foundObjects === false) {

                        foundObjects = true;
                        object.name = name;

                    } else {

                        addObject(name);

                    }

                } else if (/^usemtl /.test(line)) {

                    // material

                    object.material.name = line.substring(7).trim();

                } else if (/^mtllib /.test(line)) {

                    // mtl file

                } else if (( result = smoothing_pattern.exec(line) ) !== null) {

                    // smooth shading

                    //object.material.smooth = result[1] === "1" || result[1] === "on";

                } else if (line.charAt(0) === '#') {

                    return;

                } else {

                    throw new Error("Unexpected line " + i + ": '" + line + "' length " + line.trim().length + " charCode" + line.charCodeAt(0));

                }
            }

            let sbuf = '';
            let pos = 0;
            let bufsize = 256 * 1024;
            onProgress(0);
            const f = function() {
                if (pos < buf.byteLength) {
                    dataView = new DataView(buf, pos, Math.min(bufsize, buf.byteLength - pos));

                    sbuf += decoder.decode(dataView);

                    let lines = sbuf.split("\n");

                    for (let i = 0; i < lines.length - 1; i++) {
                        let line = lines[i];
                        line = line.trim();
                        parseLine(line);
                    }

                    sbuf = lines[lines.length - 1];
                    pos += bufsize;
                    onProgress(pos / buf.byteLength);
                    setTimeout(f, 0);
                } else {
                    onProgress(1);
                    buf = null;

                    resolve(objects);
                }
            }
            f();

        }).catch((error) => { throw error })

    })

}