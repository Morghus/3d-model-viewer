require('./progress/style.less')
import Component from '../lib/Component'
import $ from 'jquery'
import * as ProgressBar from 'progressbar.js'

export default class extends Component {
    constructor(domElement) {
        super()

        this.domElement = domElement
        this.containerDiv = $('<div class="progress-main">');
        $(domElement).append(this.containerDiv)

        this.progress = new ProgressBar.Circle($('<div>').appendTo(this.containerDiv).get(0), {
            color: '#aaa',
            trailColor: '#000',
            strokeWidth: 10,
            duration: 2500,
            easing: 'easeInOut',
            text: {
                value: 0
            },
            step: function(state, bar) {
                bar.setText((bar.value() * 100).toFixed(1) + '%')
            }
        });
    }

    remove() {
        if (this.progress) {
            this.progress.destroy()
            this.progress = null
            this.containerDiv.remove();
        }
    }

    update(value) {
        this.progress.set(value)
    }

    hide() {
        if (this.progress) {
            this.containerDiv.addClass('fade-out')
            setTimeout(() => this.remove(), 1500)
        }
    }
}