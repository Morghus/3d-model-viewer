import dat from 'dat-gui'
import THREE from 'three'
import URI from 'urijs'
import Progress from './ui/progress'
import $ from 'jquery'

import Viewer from './ModelViewer'
import dialogPolyfill from 'dialog-polyfill'
import OrbitCameraBehavior from './behaviors/OrbitCameraBehavior'
import KeyboardControls from './behaviors/KeyboardControls'
import SceneManager from './scenes/SceneManager'
import {XHRError} from "./utils/ModelLoader";

//region CSS
require('../node_modules/dialog-polyfill/dialog-polyfill.css')
require('../node_modules/material-design-lite/material.min.css')
require('./app.less')
//endregion

let queryParams = new URI().search(true);


let viewer = new Viewer();
let progressDisplay = new Progress(document.body)
viewer.addBehavior(new OrbitCameraBehavior())
viewer.addBehavior(new KeyboardControls())
viewer.addBehavior(new SceneManager())
viewer.loadModel('data/models/' + queryParams.model, (progress) => {
    let percent = progress.loaded / progress.total;
    progressDisplay.update(percent)
}).then(() => {
    progressDisplay.update(1)
    progressDisplay.hide()
    if (typeof queryParams.controls !== 'undefined') {
        let material = {
            fov: 2
        };

        let gui = new dat.GUI()
        gui.add(material, 'fov', 1, 80).onChange((value) => {
            ((position, oldFov, newFov) => {
                let oldLength = position.length();
                let newLength = oldLength / Math.tan(newFov / 2 * (Math.PI / 180)) * Math.tan(oldFov / 2 * (Math.PI / 180))
                position.multiplyScalar(newLength / oldLength)
                viewer.camera.fov = newFov
                viewer.camera.updateProjectionMatrix()
            })(viewer.camera.position, viewer.camera.fov, value)

        })
    }
    viewer.start()
})
.catch((error) => {
    console.error(error)
    error instanceof XHRError && console.log(error.xhr)
    progressDisplay.remove()
    let dialog = $(`
<dialog class="mdl-dialog">
    <h3 class="mdl-dialog__title">Sorry...</h3>
    <div class="mdl-dialog__content">
        <p>
            ${error}
        <p>
    </div>
</dialog>
    `).prependTo(document.body).get(0)

    if (!dialog.showModal) {
        dialogPolyfill.registerDialog(dialog);
    }
    dialog.showModal()
})






