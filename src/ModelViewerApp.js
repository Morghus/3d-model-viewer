import Component from "./lib/Component";
import Viewer from './ModelViewer'
import OrbitCameraBehavior from "./behaviors/OrbitCameraBehavior"
import KeyboardControls from "./behaviors/KeyboardControls"
import SceneManager from "./scenes/SceneManager"

/**
 * @property {ModelViewer} viewer
 */
export default class ModelViewerApp extends Component {
    constructor(modelPath) {
        super()
        this.modelPath = modelPath
        const viewer = this.viewer = new Viewer();
        viewer.addBehavior(new OrbitCameraBehavior())
        viewer.addBehavior(new KeyboardControls())
        viewer.addBehavior(new SceneManager())
    }
    
    load(onProgress) {
        return new Promise((resolve, reject) => {
            this.viewer.loadModel(this.modelPath, onProgress)
                .then(resolve)
                .catch(reject)
        })
    }
    
    start() {
        this.viewer.start()
    }
}

module.exports = ModelViewerApp